addLibPath("/Users/jchurchill2016/Documents/Python_Basics/Week6/")
from pet import *


myPet = Pet("Fido","Dog",5)
print("The name of my pet is", myPet.get_name())
print("The age of my pet is", str(myPet.get_age()))
print("The type of pet it is is", myPet.get_animal_type())
newName = raw_input("Enter a new name for your pet: ")
newAge = raw_input("Enter a new age for your pet: ")
newType = raw_input("Enter a new type of pet: ")
myPet.set_name(newName)
myPet.set_age(newAge)
myPet.set_animal_type(newType)
print("The new name of your pet is", myPet.get_name())
print("The new age of your pet is", myPet.get_age())
print("The new type of your pet is", myPet.get_animal_type())