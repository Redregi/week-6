addLibPath("/Users/jchurchill2016/Documents/Python_Basics/Week6/")
from trivia import *

def qOne():
  nC = 0
  print("Player One, start!")
  questionOne = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qOtQ = "Who is the biggest goblin?"
  qOp1 = "Hammerhead"
  qOp2 = "Iain"
  qOp3 = "Snake Boots"
  qOp4 = "Santa"
  qOnC = "D"
  
  questionOne.set_tQ(qOtQ)
  questionOne.set_p1(qOp1)
  questionOne.set_p2(qOp2)
  questionOne.set_p3(qOp3)
  questionOne.set_p4(qOp4)
  questionOne.set_nC(qOnC)
  
  print(questionOne.get_tQ())
  print("A. ", questionOne.get_p1())
  print("B. ", questionOne.get_p2())
  print("C. ", questionOne.get_p3())
  print("D. ", questionOne.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qOnC):
    nC += 1
    
  questionTwo = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTtQ = "What is the greatest threat to American freedom?"
  qTp1 = "Caves"
  qTp2 = "Plateaus"
  qTp3 = "Coniferous Trees"
  qTp4 = "Screaming"
  qTnC = "B"
  
  questionTwo.set_tQ(qTtQ)
  questionTwo.set_p1(qTp1)
  questionTwo.set_p2(qTp2)
  questionTwo.set_p3(qTp3)
  questionTwo.set_p4(qTp4)
  questionTwo.set_nC(qTnC)
  
  print(questionTwo.get_tQ())
  print("A. ", questionTwo.get_p1())
  print("B. ", questionTwo.get_p2())
  print("C. ", questionTwo.get_p3())
  print("D. ", questionTwo.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTnC):
    nC += 1
  
  questionThree = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTHtQ = "Why?"
  qTHp1 = "Because."
  qTHp2 = "Cause."
  qTHp3 = "Why not?"
  qTHp4 = "In 1914 Arch Duke Franz Ferdinand was assassinated."
  qTHnC = "A"
  
  questionThree.set_tQ(qTHtQ)
  questionThree.set_p1(qTHp1)
  questionThree.set_p2(qTHp2)
  questionThree.set_p3(qTHp3)
  questionThree.set_p4(qTHp4)
  questionThree.set_nC(qTHnC)
  
  print(questionThree.get_tQ())
  print("A. ", questionThree.get_p1())
  print("B. ", questionThree.get_p2())
  print("C. ", questionThree.get_p3())
  print("D. ", questionThree.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTHnC):
    nC += 1
  
  questionFour = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFtQ = "How many teeth does Peyton have?"
  qFp1 = "Enough"
  qFp2 = "Plenty."
  qFp3 = "The regular amount."
  qFp4 = "15."
  qFnC = "C"
  
  questionFour.set_tQ(qFtQ)
  questionFour.set_p1(qFp1)
  questionFour.set_p2(qFp2)
  questionFour.set_p3(qFp3)
  questionFour.set_p4(qFp4)
  questionFour.set_nC(qFnC)
  
  print(questionFour.get_tQ())
  print("A. ", questionFour.get_p1())
  print("B. ", questionFour.get_p2())
  print("C. ", questionFour.get_p3())
  print("D. ", questionFour.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFnC):
    nC += 1

  questionFive = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFItQ = "RIDE INTO VALHALLA, SHINY AND CHROME"
  qFIp1 = "PRAISE BE!"
  qFIp2 = "FOR THE EMPEROR!"
  qFIp3 = "FOR THE COVENANT!"
  qFIp4 = "DONT GET ADDICTED TO WATER!"
  qFInC = "D"
  
  questionFive.set_tQ(qFItQ)
  questionFive.set_p1(qFIp1)
  questionFive.set_p2(qFIp2)
  questionFive.set_p3(qFIp3)
  questionFive.set_p4(qFIp4)
  questionFive.set_nC(qFInC)
  
  print(questionFive.get_tQ())
  print("A. ", questionFive.get_p1())
  print("B. ", questionFive.get_p2())
  print("C. ", questionFive.get_p3())
  print("D. ", questionFive.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFInC):
    nC += 1
  print("Congratulations! Player One got ", nC, " correct!")

  

def qTwo():
  nC = 0
  print("Player Two, start!")
  questionOne = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qOtQ = "What is the biggest?"
  qOp1 = "Hydrogen Atom."
  qOp2 = "Bean."
  qOp3 = "Size is relative, this question has no meaning."
  qOp4 = "Kanye's Ego"
  qOnC = "D"
  
  questionOne.set_tQ(qOtQ)
  questionOne.set_p1(qOp1)
  questionOne.set_p2(qOp2)
  questionOne.set_p3(qOp3)
  questionOne.set_p4(qOp4)
  questionOne.set_nC(qOnC)
  
  print(questionOne.get_tQ())
  print("A. ", questionOne.get_p1())
  print("B. ", questionOne.get_p2())
  print("C. ", questionOne.get_p3())
  print("D. ", questionOne.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qOnC):
    nC += 1
    
  questionTwo = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTtQ = "Who is underground?"
  qTp1 = "Hipster bands."
  qTp2 = "Mole people."
  qTp3 = "Birds."
  qTp4 = "Good."
  qTnC = "B"
  
  questionTwo.set_tQ(qTtQ)
  questionTwo.set_p1(qTp1)
  questionTwo.set_p2(qTp2)
  questionTwo.set_p3(qTp3)
  questionTwo.set_p4(qTp4)
  questionTwo.set_nC(qTnC)
  
  print(questionTwo.get_tQ())
  print("A. ", questionTwo.get_p1())
  print("B. ", questionTwo.get_p2())
  print("C. ", questionTwo.get_p3())
  print("D. ", questionTwo.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTnC):
    nC += 1
  
  questionThree = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qTHtQ = "Hoo-"
  qTHp1 = "-Bistank."
  qTHp2 = "-Are you?"
  qTHp3 = "-ooooooo."
  qTHp4 = "-rton hears a hoo."
  qTHnC = "A"
  
  questionThree.set_tQ(qTHtQ)
  questionThree.set_p1(qTHp1)
  questionThree.set_p2(qTHp2)
  questionThree.set_p3(qTHp3)
  questionThree.set_p4(qTHp4)
  questionThree.set_nC(qTHnC)
  
  print(questionThree.get_tQ())
  print("A. ", questionThree.get_p1())
  print("B. ", questionThree.get_p2())
  print("C. ", questionThree.get_p3())
  print("D. ", questionThree.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qTHnC):
    nC += 1
  
  questionFour = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFtQ = "How good is bottle harsher?"
  qFp1 = "Very good."
  qFp2 = "Pretty good."
  qFp3 = "Excellent"
  qFp4 = "15."
  qFnC = "C"
  
  questionFour.set_tQ(qFtQ)
  questionFour.set_p1(qFp1)
  questionFour.set_p2(qFp2)
  questionFour.set_p3(qFp3)
  questionFour.set_p4(qFp4)
  questionFour.set_nC(qFnC)
  
  print(questionFour.get_tQ())
  print("A. ", questionFour.get_p1())
  print("B. ", questionFour.get_p2())
  print("C. ", questionFour.get_p3())
  print("D. ", questionFour.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFnC):
    nC += 1

  questionFive = Trivia("N/A", "N/A", "N/A", "N/A", "N/A", nC)
  qFItQ = "Frigid Snobold."
  qFIp1 = "Thats fine."
  qFIp2 = "Normal one is better."
  qFIp3 = "I'm illeterate"
  qFIp4 = "Depends on what you need."
  qFInC = "D"
  
  questionFive.set_tQ(qFItQ)
  questionFive.set_p1(qFIp1)
  questionFive.set_p2(qFIp2)
  questionFive.set_p3(qFIp3)
  questionFive.set_p4(qFIp4)
  questionFive.set_nC(qFInC)
  
  print(questionFive.get_tQ())
  print("A. ", questionFive.get_p1())
  print("B. ", questionFive.get_p2())
  print("C. ", questionFive.get_p3())
  print("D. ", questionFive.get_p4())
  
  answer = raw_input("What is the correct answer? ")
  if(answer == qFInC):
    nC += 1
  print("Congratulations! Player Two got ", nC, " correct!")

  

  
    
  
    
    
    
    
    


qOne()
qTwo()


  
  
  